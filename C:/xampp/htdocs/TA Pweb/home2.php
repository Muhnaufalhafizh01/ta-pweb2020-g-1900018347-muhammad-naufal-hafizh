<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/homephp2.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
</head>
<body>
    <script src="js/about.js"></script>
    <nav>
        <ul>
            <li><a href="about.php">About</a></li>
            <li><a href="homemain.php">Home</a></li>
            <div class="dropdown">
                <button onclick="myFunction()" class="dropbtn">PHP</button>
                <div id="myDropdown" class="dropdown-content">
                    <li><a href="home.php">PHP</a></li>
                    <li><a href="home2.php">PHP2</a></li>
                    <li><a href="home3.php">PHP3</a></li>
                    <li><a href="home4.php">PHP4</a></li>
                </div>
              </div>
            <li class="log1"><input type="button" class="log" value="SIGN IN" onclick="logout()"></li>
        </ul>
    </nav>
    
    <div class="hitcounter">
      <div class="warper1">
        <h1 class="h1a">Visit Count</h1>
        <div class="data">
          <?php

              $filecounter = "counter.txt";
              $fi=fopen($filecounter, "r+");

              $hit = fread($fi, filesize($filecounter));

              echo("<h2>$hit</h2    >");

              fclose($fi);

              $fi = fopen($filecounter, "w+");

              $hit = $hit+1;

              fwrite($fi,$hit,strlen($hit));

              fclose($fi);
          ?>
        </div>
      </div>
    </div>

    <div class="header">
        <div class="judul">
            <h1>PHP Dasar</h1>
        </div>
        <div class="data1">
          <h2>Percabangan</h2>
          <p>jika nilai anda 90 maka ?</p>
          <?php

            $nilai = 90;

            if ($nilai >= 90) {

              echo "Anda Lulus";

            }elseif ($nilai <= 90){

              echo "Tidak lulus";
            }

          ?>

        </div>

        <div class="data2">
          <h2>PERULANGAN</h2>
          <p>Mencetak angka 1 - 20</p>
          <?php

          for ($i=0; $i <= 20 ; $i++) {

            echo "$i      ";
          }

           ?>
        </div>
    </div>

    <div class="footer">
        <center>
            <p class="copy">Copyright 2020 by MnaufalH</p>
        </center>

    </div>
</body>
</html>
