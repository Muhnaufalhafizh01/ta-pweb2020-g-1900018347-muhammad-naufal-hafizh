<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/about.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About</title>
</head>
<body>
    <script src="js/about.js"></script>
    <nav>
        <ul>
            <li><a a class="aktif" href="about.php">About</a></li>
            <li><a href="homemain.php">Home</a></li>
            <div class="dropdown">
                <button onclick="myFunction()" class="dropbtn">PHP</button>
                <div id="myDropdown" class="dropdown-content">
                    <li><a href="home.php">PHP</a></li>
                    <li><a href="home2.php">PHP2</a></li>
                    <li><a href="home3.php">PHP3</a></li>
                    <li><a href="home4.php">PHP4</a></li>
                </div>
              </div>
            <li class="log1"><input type="button" class="log" value="SIGN IN" onclick="logout()"></li>
        </ul>
    </nav>
    
    <div class="hitcounter">
      <div class="warper1">
        <h1 class="h1a">Visit Count</h1>
        <div class="data">
          <?php

              $filecounter = "counter.txt";
              $fi=fopen($filecounter, "r+");

              $hit = fread($fi, filesize($filecounter));

              echo("<h2>$hit</h2    >");

              fclose($fi);

              $fi = fopen($filecounter, "w+");

              $hit = $hit+1;

              fwrite($fi,$hit,strlen($hit));

              fclose($fi);

          ?>
        </div>
      </div>
    </div>

    <div class="header">
        <div class="judul">
            <h1>BIODATA</h1>
        </div>
        <div class="isi">
            <div class="foto">
                <h3>FOTO</h3>
            </div>

            <div class="isi">
                <p>Nama<span style="margin-left: 17px">: </span>Muhammad Naufal Hafizh</p>
                <p>NIM <span style="margin-left: 21px">: </span>1900018347</p>
                <p>Kelas <span style="margin-left: 15px">: </span> G</p>
                <p>HOBI</p>
                <div class="border1">
                    <p class="pbor1">Mendaki Gunung Dan Mengoleksi Mainan</p>
                </div>
                <p>PRESTASI</p>
               <div class="border2">
                <p class="pbor2">Untuk Saat Ini Belum Ada Prestasi Yang Membanggakan :)</p>
               </div>
            </div>

        </div>
    </div>

    <div class="footer">
        <center>
            <p class="copy">Copyright 2020 by MnaufalH</p>
        </center>

    </div>
</body>
</html>
