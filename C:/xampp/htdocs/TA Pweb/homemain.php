<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/home.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About</title>
</head>
<body>
    <script src="js/about.js"></script>
    <nav>
        <ul>
            <li><a href="about.php">About</a></li>
            <li><a a class="aktif" href="homemain.php">Home</a></li>
            <div class="dropdown">
                <button onclick="myFunction()" class="dropbtn">PHP</button>
                <div id="myDropdown" class="dropdown-content">
                    <li><a href="home.php">PHP</a></li>
                    <li><a href="home2.php">PHP2</a></li>
                    <li><a href="home3.php">PHP3</a></li>
                    <li><a href="home4.php">PHP4</a></li>
                </div>
              </div>
           
            <li class="log1"><input type="button" class="log" value="SIGN IN" onclick="logout()"></li>
        </ul>
    </nav>

    <div class="hitcounter">
      <div class="warper1">
        <h1 class="h1a">Visit Count</h1>
        <div class="data">
          <?php

              $filecounter = "counter.txt";
              $fi=fopen($filecounter, "r+");

              $hit = fread($fi, filesize($filecounter));

              echo("<h2>$hit</h2    >");

              fclose($fi);

              $fi = fopen($filecounter, "w+");

              $hit = $hit+1;

              fwrite($fi,$hit,strlen($hit));

              fclose($fi);

          ?>
        </div>
      </div>
    </div>

    <div class="header">
        <div class="judul">
            <h1>Home</h1>
        </div>

        <div class="isi">
            <h3>Perulangan</h3>
           <form action="" name="input">
                <div>
                    <label for="">Kalimat <span style="margin-left: 40px">: </span></label>
                    <input type="text" name="kal">
                </div>

                <div>
                    <label for="">Jumlah Ulang : </label>
                    <input type="text" name="jumlah">
                </div>

                <div>
                    <input type="button" value="SUBMIT" onclick="submit1()">
                </div>

                <label for="" id="ulang" class="total" name="ulang"></label> <br> <br>

           </form>
        </div>
    </div>

    <div class="footer">
        <center>
            <p class="copy">Copyright 2020 by MnaufalH</p>
        </center>

    </div>
</body>
</html>
